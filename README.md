# company-zsh

company backend for Zsh completions

This is a follow-up work of [`coc-zsh`](https://github.com/tjdevries/coc-zsh)

## Credit

Again, I didn't write `capture.zsh`. All credit goes to Valodim.

Also, shoutouts to tjdevries who motivated me into re-wrapping this script for this package

## Install

Download the code where Emacs can see it, then add this to your `init.el`:

```elisp
(add-to-list 'load-path "/some/path/to/company-zsh/") ;; mind that this is *not* company-zsh.el path
(require 'company-zsh)
(company-zsh-setup)
```
## License

Since some of the code is copied from coc-zsh (which was in itself copied from github.comValodim/zsh-capture-completion) this repo has two licenses:
- capture.zsh is licensed under MIT ( see https://github.com/Valodim/zsh-capture-completion/blob/master/LICENSE )
- The rest of the files (namely company-zsh.el) is licensed is licensed under GPL v3.0 or later

## Examples (inspired from coc-zsh)

Works with lots of commands (if you have completion for them)

![git_completion](./media/company-zsh-git.png)


Works with aliases!

![alias_completion](./media/company-zsh-alias.png)

Works with complex commands!

![complex_competion](./media/company-zsh-complex.png)

## Design

When a buffer is setup, a "remote" copy of current buffer is periodically updated to stay in sync.

The "remoteness" of a buffer makes sense in Tramp buffers. In this scenario, a Tramp buffer content is local and the original file is in a remote host.

### But why a remote copy?

I had to do a very convoluted work to make this package usable in Tramp buffers and to keep Emacs as snappy as possible. See docstring for `company-zsh--timer-to-keep-remote-copy-updated` 
