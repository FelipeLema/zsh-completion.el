;;; zsh-completion.el --- Zsh-powered completion-at-point-function for zsh scripts  -*- lexical-binding: t; -*-

;; Copyright © 2021  Felipe Lema

;; Author: Felipe Lema <felipelema@mortemale.org>
;; Keywords: tools, unix, convenience, extensions
;; Package-Requires: ((emacs "26.1") (async "1.9.4") (f "0.19.0") (s "1.12.0") (company "0.9.13"))
;; URL: https://codeberg.org/FelipeLema/zsh-completion
;; Version: 1.0

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; company backend for Zsh completions
;; This is a follow-up work of https://github.com/tjdevries/coc-zsh

;;; Code:
(require 'cl-lib)
(require 'company)
(require 'f)
(require 'pcase)
(require 's)
(require 'subr-x)

;;;; customs
(defgroup zsh-completion nil "Completion backend that uses zsh completion tools."
  :group 'tools)

(defcustom zsh-completion-command
  "zsh"
  "Command to run zsh.

In Tramp buffers and such it's best to use a command in PATH and not a full
path."
  :type 'string
  :safe t
  :local t
  :group 'zsh-completion)

(defcustom zsh-completion-non-zero-return-reporter
  'ignore
  "Function to report when running capture.zsh returns non-zero.

Can be `message' or `error' or something like that."
  :type 'functionp
  :group 'zsh-completion)

(defcustom zsh-completion-use-ssh-controlmaster-options-in-async
  nil
  "Use `tramp-use-ssh-controlmaster-options' in async calls.

See https://github.com/jwiegley/emacs-async/issues/98#issuecomment-642729773"
  :type 'boolean
  :group 'zsh-completion)

(defcustom zsh-completion-timeout-to-update-remote-copy
  5
  "Idle seconds before updating remote copy."
  :type 'integer
  :group 'zsh-completion)

;;;; hooks
(defvar zsh-completion-after-setup-hooks
  nil
  "Run after this buffer has been set up with `zsh-completion-setup-this-buffer'.")

;;;; constants
(defconst zsh-completion--remote-copy-prefix
  ".zsh-completion-script")

(defconst zsh-completion--max-untouched-copy
  (* 2 ;; days
     24 ;; hours
     60 ;; minutes
     60) ;; seconds
  "Time in seconds that is max allowed for a remote copy to stay alive.")
(defconst zsh-completion--capture.zsh-location
  (f-join
   (f-dirname
    (or
     (locate-library "zsh-completion")
     (and load-file-name
          ;; File is being loaded.
          load-file-name)
     ;; File is being evaluated using, for example, `eval-buffer'.
     buffer-file-name))
   "capture.zsh"))

;;;; local variables
(defvar-local zsh-completion--capture-path
  nil
  "Points to this buffer's capture.zsh file.

The file is created in `zsh-completion--ensure-buffer-setup-for-completion' and
removed within `zsh-completion--timer-to-keep-remote-copy-updated'.

This variable is set a little bit after opening the buffer.
If developers need to test whether remote copies have been set up, this
can be used and it is recommended to do so.")

(defvar-local zsh-completion--remote-copy-path
  nil
  "Path to remote copy of this buffer.")
(defvar-local zsh-completion--timer-to-keep-remote-copy-updated
  nil
  "Timer to keep \"stdin input to capture.zsh\" in sync with current buffer.

stdin input would be the current buffer content

There's a chain of characteristics that end up into us having to keep a \"local
cache\" copy in remote host:
- capture.zsh works Ok with no input, but works better with current buffer
- we can't do `process-file' with a region or a string as stdin (at least, not
  without Tramp support)
- Doing `process-file' doesn't freeze Emacs by default (or, at least, is
  barely noticeable), but doing anything else region-related will (namely
  `start-file-process')
- Using `async' to do `process-file' + copying current buffer as a remote file
  won't block, but will take a long time in setting up a Tramp connection from
  scratch

Best trade-off for something useable is keeping a remote copy up to date using
a timer")

(defvar-local zsh-completion--should-cleanup-this-buffer
  nil
  "Whether we should force `zsh-completion--cleanup-this-buffer'.")

;;;; utility functions
(cl-defun zsh-completion--async-start (callable
                                       &optional
                                       (callback 'ignore)
                                       (running-async-name ""))
  "Common set up `async-start'.

Like `async-start', this ones receives a CALLABLE sexp.
Unlike `async-start', this one uses `ignore' as default CALLBACK.
Also, may receive name for the buffer.  I work a lot with `async' and I tend to
have lots of \"*emacs*\" buffers lingering around, so naming an
async-result buffer as RUNNING-ASYNC-NAME helps a lot."
  (let ((running-async
         (async-start
          `(lambda ()
             (let ((load-path (quote ,load-path))
                   (tramp-remote-path
                    (quote ,tramp-remote-path))
                   (tramp-remote-process-environment
                    (quote
                     ,tramp-remote-process-environment))
                   (tramp-use-ssh-controlmaster-options
                    ,(and
                      zsh-completion-use-ssh-controlmaster-options-in-async
                      tramp-use-ssh-controlmaster-options)))
               (funcall
                ,callable)))
          callback)))
    ;; exit without asking if should kill this process
    (set-process-query-on-exit-flag
     running-async
     nil)
    (unless (s-blank? running-async-name)
      (with-current-buffer (process-buffer running-async)
        (rename-buffer running-async-name t)))
    ;; return running process
    running-async))

;;;; setup/cleanup remote copy functions
(defun zsh-completion--setup-remote-copy-path-then (callback)
  "Setup a unique path that will hold the copy of current buffer's content.

After setup is done, call CALLBACK with the unique path as argument.

If `default-directory' is not writeable, CALLBACK will receive a nil path."
  (let* ((syncing-zsh-buffer (current-buffer))
         (syncing-zsh-directory
          (substring-no-properties
           (with-current-buffer syncing-zsh-buffer
             default-directory))))
    (zsh-completion--async-start ;; first, get a unique session path for `zsh-completion--remote-copy-path'
     `(lambda ()
        (when (file-writable-p ,syncing-zsh-directory)
          (concat
           (file-name-as-directory
            ,syncing-zsh-directory)
           (let ((default-directory user-emacs-directory))
             ;; could use mktemp, but this is faster
             (shell-command-to-string
              ,(s-join
                " | "
                (list
                 (format
                  "printf %s-%%s $(date --iso-8601=ns"
                  zsh-completion--remote-copy-prefix)
                 "sha256sum"
                 "cut -d \\  -f 1)")))))))
     callback)))

(defun zsh-completion--cleanup-old-remnants ()
  "Remove any previous copies that are too old and are lingering around.

These copies are most likely left behind because of Emacs shutting down without
triggering hooks or because async got killed before completion."
  (zsh-completion--async-start
   `(lambda ()
      (require 'f)
      (require 's)
      (let ((default-directory ,
              (substring-no-properties
               default-directory)))
        (dolist (remote-copy-path
                 (f-files default-directory
                          (lambda (fpath)
                            (s-starts-with?
                             ,zsh-completion--remote-copy-prefix
                             (file-name-nondirectory
                              fpath)))))
          (let* ((file-last-mod-datetime
                  (nth 5
                       (file-attributes remote-copy-path)))
                 (last-mod-+-max-allowed
                  (time-add
                   file-last-mod-datetime
                   ,zsh-completion--max-untouched-copy))
                 (should-delete
                  ;; (file-date + max) < now
                  (time-less-p last-mod-+-max-allowed
                               (current-time))))

            (when should-delete
              (f-delete remote-copy-path))))))
   'ignore
   (format "* cleaning at %s *" default-directory)))


(cl-defun zsh-completion--start-remote-copy-syncing-timer (&optional
                                                           (syncing-zsh-buffer
                                                            (current-buffer)))
  "Launch first remote copy sync & setup idle timer to launch subsequent syncs.

Setup is done in SYNCING-ZSH-BUFFER.

If could not set up a remote copy file using
`zsh-completion--setup-remote-copy-path-then', then this function won't setup
timers or anything."
  (zsh-completion--setup-remote-copy-path-then
   (lambda (remote-copy-path)
     (when (and remote-copy-path
                (buffer-live-p syncing-zsh-buffer))
       (with-current-buffer syncing-zsh-buffer
         ;; got a remote unique path, let's save it in this buffer
         (setq zsh-completion--remote-copy-path
               (substring-no-properties remote-copy-path))
         ;; now thot we got a remote path, let's set the timer to keep its content
         ;; synced with `syncing-zsh-buffer' (current-buffer)
         (let* ((;; capture this timer in the (recurrent) closure so it can cancel
                 ;; itself regardless of whether the original buffer is alive or
                 ;; not
                 this-timer)
                (this-buffer
                 syncing-zsh-buffer)
                (this-capture-path)
                (currently-sending-input)
                (this-remote-copy-path
                 (copy-sequence zsh-completion--remote-copy-path))
                (updater-or-cleaner
                 ;; code to execute once now and every "N idle seconds"
                 (lambda ()
                   (if (or (buffer-live-p this-buffer)
                           zsh-completion--should-cleanup-this-buffer)
                       (with-current-buffer this-buffer
                         ;; make sure `zsh-completion--capture-path' has been
                         ;; captured in this closure
                         (unless this-capture-path
                           (setq
                            this-capture-path
                            (and zsh-completion--capture-path
                                 (copy-sequence zsh-completion--capture-path))))
                         ;; update remote copy with current buffer
                         ;; only when we're not currently already sending it
                         (unless currently-sending-input
                           (setq currently-sending-input t)
                           (zsh-completion--async-start
                            `(lambda ()
                               (require 'f)
                               (f-write-text
                                ,(or (buffer-string) "")
                                (quote ,buffer-file-coding-system)
                                ,this-remote-copy-path)
                               (set-file-modes ,this-remote-copy-path #o600)
                               t)
                            (lambda (_)
                              ;; done sending input
                              (setq currently-sending-input nil))
                            (format " * updating zsh input at %s*"
                                    this-buffer))))
                     ;; buffer not alive? cleanup then
                     (zsh-completion--cleanup-this-buffer
                      ;; captured timers and paths afterwards
                      this-timer
                      this-capture-path
                      this-remote-copy-path)))))
           ;; launch once the first time
           (funcall updater-or-cleaner)
           ;; all subsequent updates will be when idle
           (setq this-timer
                 (run-with-idle-timer ;;run-at-time
                  zsh-completion-timeout-to-update-remote-copy
                  t ;; repeat: yup
                  updater-or-cleaner))
           (setq
            zsh-completion--timer-to-keep-remote-copy-updated
            this-timer))))))
  ;; set timer to non-nil value so it won't be started twice
  (with-current-buffer syncing-zsh-buffer
    (setq zsh-completion--timer-to-keep-remote-copy-updated
          "starting")))

(cl-defun zsh-completion--cleanup-this-buffer
    (&optional
     (this-timer zsh-completion--timer-to-keep-remote-copy-updated)
     (this-capture-path zsh-completion--capture-path)
     (this-remote-copy-path zsh-completion--remote-copy-path))
  "Cleanup timers and (remote) paths.
Note: while we could obtain the values from buffer, this function could get
called _after_ the buffer has been killed.  So when calling this function you
can provide the values on your own, rather than relying on buffer-local values."
  (when (and (eq major-mode 'sh-mode)
             (eq sh-shell 'zsh))
    ;; cleanup 1 : cancel timer
    (when (timerp this-timer)
      (cancel-timer this-timer))
    ;; cleanup 2 : delete path for remote copies
    (let ((running-cleanup
           (zsh-completion--async-start
            `(lambda ()
               (require 'f)
               (dolist (remote-path (list
                                     ,this-capture-path
                                     ,this-remote-copy-path))
                 (when (and remote-path
                            (file-exists-p remote-path))
                   (f-delete remote-path))))
            'ignore "*zsh-completion: cleanup*")))
      (set-process-query-on-exit-flag running-cleanup nil))))

(defun zsh-completion--ensure-buffer-setup-for-completion ()
  "Setup timer to keep remote copy up-to-date."
  (zsh-completion--cleanup-old-remnants)
  (let ((buffer-being-setup (current-buffer)))
    (setq ;; same as `setq-local'
     zsh-completion--timer-to-keep-remote-copy-updated
     (or zsh-completion--timer-to-keep-remote-copy-updated
         (zsh-completion--start-remote-copy-syncing-timer buffer-being-setup)))
    ;; create capture.zsh on remote
    (unless zsh-completion--capture-path
      (zsh-completion--setup-remote-capture.zsh))))


(defun zsh-completion--setup-remote-capture.zsh ()
  "Keep a remote copy of capture.zsh for this buffer."
  (let ((this-directory
         (substring-no-properties default-directory))
        (buffer-being-setup (current-buffer)))
    (zsh-completion--async-start
     `(lambda ()
        (require 'f)
        (let* ((default-directory ,this-directory)
               (remote-script-path
                (concat
                 (file-remote-p ,this-directory)
                 (f-join
                  (string-trim
                   (shell-command-to-string "/bin/mktemp -d --tmpdir=/tmp capture.zsh.XXXXXXXXX"))
                  "capture.zsh"))))
          (shell-command-to-string
           (format "touch %s"
                   (shell-quote-argument
                    (file-local-name
                     remote-script-path))))
          ;; send bytes
          (f-write-text
           (f-read-text
            ;; PWD/capture.zsh
            ,zsh-completion--capture.zsh-location)
           (quote ,buffer-file-coding-system)
           remote-script-path)

          ;; keep track of this script using buffer-local var
          remote-script-path))
     (lambda (remote-script-path)
       ;; update `zsh-completion--capture-path' locally
       (when (buffer-live-p buffer-being-setup)
         (with-current-buffer buffer-being-setup
           (setq zsh-completion--capture-path
                 (substring-no-properties
                  remote-script-path)))))
     (format " * zsh-completion: setting up capture.zsh at %s"
             (buffer-name
              buffer-being-setup)))))

(defun zsh-completion--buffer-is-zsh ()
  "Tell if current buffer is zsh buffer."
  (and (eq major-mode 'sh-mode)
       (eq sh-shell 'zsh)))


(defun zsh-completion--get-completions (completing-line)
  "Feed current line as CLI arguments of capture.zsh.

COMPLETING-LINE would be the current line in buffer.
CALLBACK is (most likely) the `company' provided
callback to handle the candidates."
  (if (null zsh-completion--capture-path)
      nil ;; not fully setup yet
    (let* ((script-output-buffer
            (get-buffer-create "*zsh-completion-script-output-buffer*")))
      (with-current-buffer script-output-buffer
        (erase-buffer))
      (let* ((returned-value
              (process-file
               "timeout"
               zsh-completion--remote-copy-path
               script-output-buffer
               nil
               "--signal=KILL"
               "5s"
               zsh-completion-command
               (file-local-name
                zsh-completion--capture-path)
               ;; no need to quote the line
               ;; it's passed as second argument as-is
               completing-line))
             (lines
              (with-current-buffer script-output-buffer
                (split-string
                 (buffer-substring-no-properties
                  (point-min)
                  (point-max))
                 "\n"
                 t))))
        (if (not (equal 0 returned-value))
            (progn
              (funcall zsh-completion-non-zero-return-reporter "zsh-completion: running script returned %d, saying \"%S\""
                       returned-value
                       (s-join "\n" lines))
              ;; report no candidates
              nil)
          (-some->> lines
            ;; clean weird line ending
            (-map 's-trim-right)
            ;; try to extract some info from completion result
            (--map
             (cond
              ((s-match (rx " -- ") it) ;; candidate -- description
               (pcase-let* ((`(,cli-arg ,description)
                             (s-split " -- " it)))
                 (put-text-property 0 1 'meta
                                    (s-trim-right ;; remove trailing ^M
                                     description)
                                    cli-arg)
                 cli-arg))
              ;; no info? then forward as-is
              (t
               it)))))))))

(defconst zsh-completion--symbol-end-re
  (rx symbol-end)
  "Regex to match symbol end.")

(defun zsh-completion--grab-symbol ()
  "Based off `company-grab-symbol'."
  (if (looking-at zsh-completion--symbol-end-re)
      (buffer-substring (point) (save-excursion (skip-syntax-backward "w_")
                                                (point)))
    (unless (and (char-after) (memq (char-syntax (char-after)) '(?w ?_)))
      "")))

(defun zsh-completion--get-prefix ()
  "Kinda works using `company-grab-symbol', but prioritises files.

Also, it will support assignments (left-hand & right-hand values) since capture.zsh does."
  (let* ((file-path-or-symbol
          (or
           (let ((comint-file-name-chars ;; fix capturing filename in zsh
                  (replace-regexp-in-string
                   (rx (any ?# ?{ ?} ?\[ ?\]))
                   ""
                   comint-file-name-chars)))
             (comint-match-partial-filename))
           (zsh-completion--grab-symbol))))
    file-path-or-symbol))

(defun zsh-completion--get-command-under-cursor ()
  "Get command line under cursor, starting at `point'.

If multi-line, get all involved lines and merge as one."
  (or
   (->> (buffer-substring-no-properties
         ;; start of  command → …
         (save-excursion
           (sh-beginning-of-command)
           (while (assoc
                   (sh-smie--default-forward-token)
                   smie-grammar)
             (funcall smie-forward-token-function))
           (funcall smie-backward-token-function)
           (point))
         ;; … → current position
         (point))
     (s-replace-regexp (rx "\\" line-end) "")
     (s-split "\n")
     (s-join " ")
     (s-trim))
   ""))

(defun zsh-completion--annotate (candidate-text)
  "Annotate CANDIDATE-TEXT if it has embedded info.

Candidate was embedded with info in `zsh-completion--get-completions'."
  (get-text-property 0 'meta
                     candidate-text))

;;;###autoload
(defun zsh-completion-at-point ()
  (when (zsh-completion--buffer-is-zsh)
    (let* ((prefix (zsh-completion--get-prefix))
           (start
            (->> prefix
              (string-bytes)
              (- (point))))
           (end
            (point))
           (candidates
            (zsh-completion--get-completions
             (zsh-completion--get-command-under-cursor))))
      (list
       start
       end
       (lambda (probe _pred action)
         (pcase action
           (`metadata
            `(metadata (category . zsh-completion-capf)
                       (display-sort-function . identity)))
           (`(boundaries . ,_)
            nil)
           (`nil ;; try-completion
            (if (cdr candidates)
                probe
              (car candidates)))
           (`lambda ;; test-completion
             nil)
           (`t ;; retreive candidates
            candidates)))
       ;; see `completion-extra-properties' for properties below
       :annotation-function #'zsh-completion--annotate
       :company-require-match 'never))))

;;;###autoload
(defun zsh-completion-setup-this-buffer ()
  "Setup this buffer for zsh completion.

Won't be setup if Emacs does not display/report this is a zsh buffer."
  (when (zsh-completion--buffer-is-zsh)
    (zsh-completion--ensure-buffer-setup-for-completion)
    ;; add local hook to cleanup after buffer is killed
    (add-hook 'kill-buffer-hook
              #'zsh-completion--cleanup-this-buffer
              nil
              t)
    (run-hooks 'zsh-completion-after-setup-hooks)))

;;;###autoload
(define-minor-mode zsh-completion-mode
  "Use zsh as backend for completion."
  :group 'zsh-completion
  :global nil
  :lighter ""
  (if zsh-completion-mode
      (progn
        (setq-local completion-at-point-functions nil)
        (add-hook 'completion-at-point-functions #'zsh-completion-at-point nil t)
        (setq-local completion-category-defaults
                    (add-to-list 'completion-category-defaults '(zsh-completion-capf (styles basic))))

        (zsh-completion-setup-this-buffer))
    (remove-hook 'completion-at-point-functions #'zsh-completion-at-point t)
    (setq-local completion-category-defaults
                (cl-remove 'zsh-completion-capf completion-category-defaults :key #'cl-first))
    ;; force `zsh-completion--cleanup-this-buffer'
    (setq zsh-completion--should-cleanup-this-buffer t)))


(provide 'zsh-completion)
;;; zsh-completion.el ends here
